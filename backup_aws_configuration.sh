#!/bin/bash

# Set the number of expected arguments to 3
EXPECTED_ARGS=3

# If we don't receive 3 arguments tell user and provide usage info.
if [ $# -ne $EXPECTED_ARGS ]; then
  echo
  echo "backup_aws_configuration.sh expects 1 arguments:			"
  echo "	S3 bucket name: surveycto.backups, etc.	"
  echo "	Primary region: us-east-1, etc.	"
  echo "	Secondary region: eu-west-1, etc.	"
  echo "Example:								 	"
  echo "	backup_aws_configuration.sh surveycto.backups us-east-1 eu-west-1"
  echo

  # Exit with error
  exec >&2
  echo "backup_aws_configuration.sh: Incorrect number of arguments ($# when $EXPECTED_ARGS expected)."
  exit 1
fi

S3_DEST_FOLDER="s3://$1/aws-configuration"
PRIMARY_REGION=$2
SECONDARY_REGION=$3

###############################################################################################################
# Action: Backup SSM documents                                                                                #
###############################################################################################################

echo "."
echo "."
echo "."
echo "."
echo "Backing up SSM documents..."
echo "."

#SSM_DOCS_BCK_DIR=ssm-document-backup-$(date +'%Y-%m-%d')
#mkdir $SSM_DOCS_BCK_DIR
#for docname in $(aws ssm list-documents --region $PRIMARY_REGION --max-items 200 --document-filter-list "key=Owner,value=self" "key=DocumentType,value=Command" --query "DocumentIdentifiers[*].Name" --output text); do
#  aws ssm get-document --region $PRIMARY_REGION --output text --name $docname >$SSM_DOCS_BCK_DIR/$docname
#done
#for docname in $(aws ssm list-documents --region $PRIMARY_REGION --max-items 200 --document-filter-list "key=Owner,value=self" "key=DocumentType,value=Automation" --query "DocumentIdentifiers[*].Name" --output text); do
#  aws ssm get-document --region $PRIMARY_REGION --output text --name $docname >$SSM_DOCS_BCK_DIR/$docname
#done
#for docname in $(aws ssm list-documents --region $SECONDARY_REGION --max-items 200 --document-filter-list "key=Owner,value=self" "key=DocumentType,value=Command" --query "DocumentIdentifiers[*].Name" --output text); do
#  aws ssm get-document --region $SECONDARY_REGION --output text --name $docname >$SSM_DOCS_BCK_DIR/EU-$docname
#done
#for docname in $(aws ssm list-documents --region $SECONDARY_REGION --max-items 200 --document-filter-list "key=Owner,value=self" "key=DocumentType,value=Automation" --query "DocumentIdentifiers[*].Name" --output text); do
#  aws ssm get-document --region $SECONDARY_REGION --output text --name $docname >$SSM_DOCS_BCK_DIR/EU-$docname
#done
#tar -zcvf $SSM_DOCS_BCK_DIR.tar.gz $SSM_DOCS_BCK_DIR/
#aws s3 cp --no-progress $SSM_DOCS_BCK_DIR.tar.gz $S3_DEST_FOLDER/ssm-documents/
#rm $SSM_DOCS_BCK_DIR.tar.gz
#rm -rf $SSM_DOCS_BCK_DIR
#aws s3 ls $S3_DEST_FOLDER/ssm-documents/$SSM_DOCS_BCK_DIR.tar.gz

echo "."
echo "Done!"

###############################################################################################################
# Action: Backup SSM parameters                                                                               #
###############################################################################################################

echo "."
echo "."
echo "."
echo "."
echo "Backing up SSM parameters..."
echo "."

#SSM_PARAMS_BCK_DIR=ssm-parameters-backup-$(date +'%Y-%m-%d')
#mkdir $SSM_PARAMS_BCK_DIR
#aws ssm get-parameters-by-path --path "/" --recursive --region "us-east-1" >$SSM_PARAMS_BCK_DIR/ssm-parameters.json
#tar -zcvf $SSM_PARAMS_BCK_DIR.tar.gz $SSM_PARAMS_BCK_DIR/
#aws s3 cp --no-progress $SSM_PARAMS_BCK_DIR.tar.gz $S3_DEST_FOLDER/ssm-parameters/
#rm $SSM_PARAMS_BCK_DIR.tar.gz
#rm -rf $SSM_PARAMS_BCK_DIR
#aws s3 ls $S3_DEST_FOLDER/ssm-parameters/$SSM_PARAMS_BCK_DIR.tar.gz

echo "."
echo "Done!"

###############################################################################################################
# Action: Backup maintenance windows                                                                          #
###############################################################################################################

echo "."
echo "."
echo "."
echo "."
echo "Backing up maintenance windows..."
echo "."

#SSM_MW_BCK_DIR=ssm-maintenance-window-backup-$(date +'%Y-%m-%d')
#mkdir $SSM_MW_BCK_DIR
#aws ssm describe-maintenance-windows --region $PRIMARY_REGION --max-items 200 >$SSM_MW_BCK_DIR/maintenance_windows.json
#aws ssm describe-maintenance-windows --region $SECONDARY_REGION --max-items 200 >$SSM_MW_BCK_DIR/EU-maintenance_windows.json
#for windowId in $(aws ssm describe-maintenance-windows --region $PRIMARY_REGION --max-items 200 --query "WindowIdentities[*].WindowId" --output text); do
#  # echo ". Window ID='$windowId':"
#  for taskId in $(aws ssm describe-maintenance-window-tasks --region $PRIMARY_REGION --max-items 200 --query "Tasks[*].WindowTaskId" --output text --window-id $windowId); do
#    # echo ". --> Task ID='$taskId'"
#    aws ssm get-maintenance-window-task --region $PRIMARY_REGION --window-id $windowId --window-task-id $taskId >$SSM_MW_BCK_DIR/window-$windowId-task-$taskId
#  done
#done
#for windowId in $(aws ssm describe-maintenance-windows --region $SECONDARY_REGION --max-items 200 --query "WindowIdentities[*].WindowId" --output text); do
#  # echo ". Window ID='$windowId':"
#  for taskId in $(aws ssm describe-maintenance-window-tasks --region $SECONDARY_REGION --max-items 200 --query "Tasks[*].WindowTaskId" --output text --window-id $windowId); do
#    # echo ". --> Task ID='$taskId'"
#    aws ssm get-maintenance-window-task --region $SECONDARY_REGION --window-id $windowId --window-task-id $taskId >$SSM_MW_BCK_DIR/EU-window-$windowId-task-$taskId
#  done
#done
#tar -zcvf $SSM_MW_BCK_DIR.tar.gz $SSM_MW_BCK_DIR/
#aws s3 cp --no-progress $SSM_MW_BCK_DIR.tar.gz $S3_DEST_FOLDER/ssm-maintenance-windows/
#rm $SSM_MW_BCK_DIR.tar.gz
#rm -rf $SSM_MW_BCK_DIR
#aws s3 ls $S3_DEST_FOLDER/ssm-maintenance-windows/$SSM_MW_BCK_DIR.tar.gz

echo "."
echo "Done!"

###############################################################################################################
# Action: Backup patch groups and baselines                                                                   #
###############################################################################################################

echo "."
echo "."
echo "."
echo "."
echo "Backing up patch groups and baselines..."
echo "."

#SSM_PG_BCK_DIR=ssm-patch-groups-backup-$(date +'%Y-%m-%d')
#mkdir $SSM_PG_BCK_DIR
#aws ssm describe-patch-groups --region $PRIMARY_REGION --max-items 200 >$SSM_PG_BCK_DIR/patch-groups.json
#aws ssm describe-patch-groups --region $SECONDARY_REGION --max-items 200 >$SSM_PG_BCK_DIR/EU-patch-groups.json
#for baselineId in $(aws ssm describe-patch-groups --region $PRIMARY_REGION --max-items 200 --query "Mappings[*].BaselineIdentity.BaselineId" --output text); do
#  # echo ". Baseline ID='$baselineId':"
#  aws ssm get-patch-baseline --region $PRIMARY_REGION --baseline-id $baselineId >$SSM_PG_BCK_DIR/baseline-$baselineId
#done
#for baselineId in $(aws ssm describe-patch-groups --region $SECONDARY_REGION --max-items 200 --query "Mappings[*].BaselineIdentity.BaselineId" --output text); do
#  # echo ". Baseline ID='$baselineId':"
#  aws ssm get-patch-baseline --region $SECONDARY_REGION --baseline-id $baselineId >$SSM_PG_BCK_DIR/baseline-$baselineId
#done
#tar -zcvf $SSM_PG_BCK_DIR.tar.gz $SSM_PG_BCK_DIR/
#aws s3 cp --no-progress $SSM_PG_BCK_DIR.tar.gz $S3_DEST_FOLDER/ssm-patch-groups/
#rm $SSM_PG_BCK_DIR.tar.gz
#rm -rf $SSM_PG_BCK_DIR
#aws s3 ls $S3_DEST_FOLDER/ssm-patch-groups/$SSM_PG_BCK_DIR.tar.gz

echo "."
echo "Done!"

###############################################################################################################
# Action: Backup VPC configuration                                                                            #
###############################################################################################################

echo "."
echo "."
echo "."
echo "."
echo "Backing up VPC configuration..."
echo "."

#VPC_CONFIG_BCK_DIR=vpc-configuration-backup-$(date +'%Y-%m-%d')
#mkdir $VPC_CONFIG_BCK_DIR
#aws ec2 describe-vpcs --region $PRIMARY_REGION  --no-paginate > $VPC_CONFIG_BCK_DIR/vpc-configuration.json
#aws ec2 describe-vpcs --region $SECONDARY_REGION  --no-paginate > $VPC_CONFIG_BCK_DIR/EU-vpc-configuration.json
#aws ec2 describe-subnets --region $PRIMARY_REGION --no-paginate > $VPC_CONFIG_BCK_DIR/vpc-subnets.json
#aws ec2 describe-subnets --region $SECONDARY_REGION --no-paginate > $VPC_CONFIG_BCK_DIR/EU-vpc-subnets.json
#aws ec2 describe-route-tables --region $PRIMARY_REGION --no-paginate > $VPC_CONFIG_BCK_DIR/vpc-route-tables.json
#aws ec2 describe-route-tables --region $SECONDARY_REGION --no-paginate > $VPC_CONFIG_BCK_DIR/EU-vpc-route-tables.json
#aws ec2 describe-internet-gateways --region $PRIMARY_REGION  --no-paginate > $VPC_CONFIG_BCK_DIR/vpc-internet-gateways.json
#aws ec2 describe-internet-gateways --region $SECONDARY_REGION  --no-paginate > $VPC_CONFIG_BCK_DIR/EU-vpc-internet-gateways.json
#aws ec2 describe-nat-gateways --region $PRIMARY_REGION  --no-paginate > $VPC_CONFIG_BCK_DIR/vpc-nat-gateways.json
#aws ec2 describe-nat-gateways --region $SECONDARY_REGION  --no-paginate > $VPC_CONFIG_BCK_DIR/EU-vpc-nat-gateways.json
#aws ec2 describe-dhcp-options --region $PRIMARY_REGION --no-paginate > $VPC_CONFIG_BCK_DIR/vpc-dhcp-options.json
#aws ec2 describe-dhcp-options --region $SECONDARY_REGION --no-paginate > $VPC_CONFIG_BCK_DIR/EU-vpc-dhcp-options.json
#aws ec2 describe-vpc-endpoints --region $PRIMARY_REGION  --no-paginate > $VPC_CONFIG_BCK_DIR/vpc-endpoints.json
#aws ec2 describe-vpc-endpoints --region $SECONDARY_REGION  --no-paginate > $VPC_CONFIG_BCK_DIR/EU-vpc-endpoints.json
#aws ec2 describe-vpc-peering-connections --region $PRIMARY_REGION  --no-paginate > $VPC_CONFIG_BCK_DIR/vpc-peering-cnnections.json
#aws ec2 describe-vpc-peering-connections --region $SECONDARY_REGION  --no-paginate > $VPC_CONFIG_BCK_DIR/EU-vpc-peering-connections.json
#aws ec2 describe-security-groups --region $PRIMARY_REGION  --no-paginate >$VPC_CONFIG_BCK_DIR/vpc-security-groups.json
#aws ec2 describe-security-groups --region $SECONDARY_REGION  --no-paginate >$VPC_CONFIG_BCK_DIR/EU-vpc-security-groups.json
#tar -zcvf $VPC_CONFIG_BCK_DIR.tar.gz $VPC_CONFIG_BCK_DIR/
#aws s3 cp --no-progress $VPC_CONFIG_BCK_DIR.tar.gz $S3_DEST_FOLDER/vpc-configuration/
#rm $VPC_CONFIG_BCK_DIR.tar.gz
#rm -rf $VPC_CONFIG_BCK_DIR
#aws s3 ls $S3_DEST_FOLDER/vpc-configuration/$VPC_CONFIG_BCK_DIR.tar.gz

echo "."
echo "Done!"

###############################################################################################################
# Action: Backup IAM policies, users and roles                                                                #
###############################################################################################################

echo "."
echo "."
echo "."
echo "."
echo "Backing up IAM policies, users, groups and roles..."
echo "."

IAM_CONFIG_DIR=iam-backup-$(date +'%Y-%m-%d')
mkdir $IAM_CONFIG_DIR

aws iam list-users --no-paginate > $IAM_CONFIG_DIR/iam-users.json
for user in $(aws iam list-users --no-paginate --query "Users[*].UserName" --output text); do
  aws iam list-user-tags --no-paginate  --user-name $user > $IAM_CONFIG_DIR/iam-user-tags-$user.json
  aws iam list-groups-for-user --no-paginate --user-name $user > $IAM_CONFIG_DIR/iam-user-groups-$user.json
  aws iam list-attached-user-policies  --no-paginate --user-name $user > $IAM_CONFIG_DIR/iam-user-attached-policies-$user.json
  aws iam list-user-policies  --no-paginate --user-name $user > $IAM_CONFIG_DIR/iam-user-inline-policies-$user.json
  for user_policy in $(aws iam list-user-policies --no-paginate --user-name $user --query "PolicyNames" --output text); do
    aws iam get-user-policy --user $user --policy-name $user_policy > $IAM_CONFIG_DIR/iam-user-inline-policy-$user-$user_policy.json
  done
done

aws iam list-groups --no-paginate > $IAM_CONFIG_DIR/iam-groups.json
for group in $(aws iam list-groups --no-paginate --query "Groups[*].GroupName" --output text); do
  aws iam list-attached-group-policies --no-paginate --group-name $group > $IAM_CONFIG_DIR/iam-group-attached-policies-$group.json
  aws iam list-group-policies --no-paginate --group-name $group > $IAM_CONFIG_DIR/iam-group-inline-policies-$group.json
  for group_policy in $(aws iam list-group-policies --no-paginate --group-name $group --query "PolicyNames" --output text); do
    aws iam get-group-policy --group $group --policy-name $group_policy > $IAM_CONFIG_DIR/iam-group-inline-policy-$group-$group_policy.json
  done
done

aws iam list-roles --no-paginate > $IAM_CONFIG_DIR/iam-roles.json
for role in $(aws iam list-roles --no-paginate --query "Roles[*].RoleName" --output text); do
  aws iam list-role-tags --no-paginate  --role-name $role > $IAM_CONFIG_DIR/iam-role-tags-$role.json
  aws iam list-attached-role-policies  --no-paginate --role-name $role > $IAM_CONFIG_DIR/iam-role-attached-policies-$role.json
  aws iam list-role-policies  --no-paginate --role-name $role > $IAM_CONFIG_DIR/iam-role-inline-policies-$role.json
  for role_policy in $(aws iam list-role-policies --no-paginate --role-name $role --query "PolicyNames" --output text); do
    aws iam get-role-policy --role $role --policy-name $role_policy > $IAM_CONFIG_DIR/iam-role-inline-policy-$role-$role_policy.json
  done
done

while read -r val ; do
	policy=($val)
  aws iam get-policy-version --policy-arn ${policy[0]} --version-id ${policy[1]} > $IAM_CONFIG_DIR/iam-managed-policy-${policy[2]}.json
done < <(aws iam list-policies --only-attached --no-paginate --query "Policies[*].[Arn,DefaultVersionId,PolicyName]" --output text);

tar -zcvf $IAM_CONFIG_DIR.tar.gz $IAM_CONFIG_DIR/
aws s3 cp --no-progress $IAM_CONFIG_DIR.tar.gz $S3_DEST_FOLDER/iam-configuration/
rm $IAM_CONFIG_DIR.tar.gz
rm -rf $IAM_CONFIG_DIR
aws s3 ls $S3_DEST_FOLDER/iam-configuration/$IAM_CONFIG_DIR.tar.gz

echo "."
echo "Done!"

